package cz.muni.fi.pa053.homework3_2;

import org.json.JSONObject;
import org.mariuszgromada.math.mxparser.Argument;
import org.mariuszgromada.math.mxparser.Constant;
import org.mariuszgromada.math.mxparser.Expression;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


@Service
public class ApiService {


    private final RestTemplate restTemplate;

    public ApiService() {
        this.restTemplate = new RestTemplate();
    }

    public String callApi(String url) {
        ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
        return response.getBody();
    }
    public String getPriceFromResponse(String jsonResponse) {
        if (jsonResponse == "{}") {
            return "No data available";
        }
        JSONObject jsonObject = new JSONObject(jsonResponse);
        String price = jsonObject.getJSONObject("Global Quote").getString("05. price");
        return price;
    }
    public String[] getLongLatFromResponse(String jsonResponse) {
        JSONObject jsonObject = new JSONObject(jsonResponse);
        String longitude = jsonObject.getString("longitude");
        String latitude = jsonObject.getString("latitude");
        return new String[]{latitude, longitude};
    }
    public double getTemperatureFromResponse(String jsonResponse) {
        JSONObject jsonObject = new JSONObject(jsonResponse);
        double tempC = jsonObject.getJSONObject("current").getDouble("temp_c");
        return tempC;
    }
    public double evaluateExpression(String expression) {
        Expression e = new Expression(expression);
        return e.calculate();
    }
}