package cz.muni.fi.pa053.homework3_2;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.script.ScriptException;
import java.util.HashMap;
import java.util.Map;

@RestController
public class Controller {

    private ApiService apiService;

    public Controller() {
        this.apiService = new ApiService();
    }
    private final String apiKey = "RIL9N80EA9ODKH9V";

    @GetMapping("/query")
    public ResponseEntity<Map<String, Object>> query(@RequestParam(required = false) String queryAirportTemp,
                                                     @RequestParam(required = false) String queryStockPrice,
                                                     @RequestParam(required = false) String queryEval) {
        Map<String, Object> response = new HashMap<>();
        String apiResponse;
        if (queryAirportTemp != null) {
            apiResponse = apiService.callApi("https://www.airport-data.com/api/ap_info.json?iata=" + queryAirportTemp);
            String[] longLatFromResponse = apiService.getLongLatFromResponse(apiResponse);
            apiResponse = apiService.callApi("https://api.weatherapi.com/v1/current.json?key=091fe4c373804ccd94392549242604&q=" + longLatFromResponse[0] + "," + longLatFromResponse[1]);

            response.put("result", apiService.getTemperatureFromResponse(apiResponse));
        } else if (queryStockPrice != null) {
            apiResponse = apiService.callApi("https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol="+ queryStockPrice +"&apikey=RIL9N80EA9ODKH9V");
            apiResponse = apiResponse != null ? apiResponse : "{}";
            response.put("result", apiService.getPriceFromResponse(apiResponse));
        } else if (queryEval != null) {
                double result = apiService.evaluateExpression(queryEval);
                response.put("result", result);
        } else {
            response.put("error", "Exactly one of queryAirportTemp, queryStockPrice, queryEval must be provided");
            return ResponseEntity.badRequest().contentType(MediaType.APPLICATION_JSON).body(response);
        }
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(response);
    }
}